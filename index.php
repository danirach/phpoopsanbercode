<?php
    require('animal.php');
    require('frog.php');
    require('ape.php');

    $sheep = new Animal("shaun");
    $sungokong = new Ape("kera sakti");
    $kodok = new Frog("buduk");
    
    echo "Name: ". $sheep->name . "<br>";
    echo "legs: ". $sheep->legs . "<br>";
    echo "cold blooded: " .$sheep->cold_blooded . "<br>";

    echo "Name: ". $sungokong->name . "<br>";
    echo "legs: ". $sungokong->legs . "<br>";
    echo "cold blooded: " .$sungokong->cold_blooded . "<br>";

    echo "Name: ". $kodok->name . "<br>";
    echo "legs: ". $kodok->legs . "<br>";
    echo "cold blooded: " .$kodok->cold_blooded . "<br>";
?>